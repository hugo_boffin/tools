﻿open System
open System.IO
open System.Linq
open System.Security.Cryptography
open FSharp.Collections.ParallelSeq

let provider = new MD5CryptoServiceProvider() // Use another hash function?

let calcCheckSum (x:FileInfo) =
    try
        let checksum = provider.ComputeHash(new BufferedStream(File.OpenRead(x.FullName), 1200000));
        BitConverter.ToString(checksum).Replace("-", String.Empty);
    with
        | :? System.IO.PathTooLongException -> printfn "Path too long! %s" x.Name; ""

let getFilesAndSums (dir : string) = 
    let dx = new DirectoryInfo(dir);
    let xs = dx.EnumerateFiles ("*", SearchOption.AllDirectories) 
    Set.ofSeq ( PSeq.map (fun x -> (calcCheckSum x, x.FullName.Substring(dx.FullName.Length+1) ) ) xs )

[<EntryPoint>]
let main argv = 
    let dirA = argv.[0]
    let dirB = argv.[1]
    printfn "%s <-> %s\n" dirA dirB

    let xs' = getFilesAndSums dirA
    let ys' = getFilesAndSums dirB

    let sames = Set.intersect xs' ys'
    let diffs = Set.difference (Set.union xs' ys') sames // Set of all possible elements minus the identical ones
    
    printfn "Equal Files: %d" (Set.count sames)
    printfn "Different Files: %d" (Set.count diffs)
    Seq.iter (fun a -> printfn "%A" a) diffs 
    
    Console.ReadKey true |> ignore
    0