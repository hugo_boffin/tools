﻿open System
open System.IO
open System.Linq
open System.Security.Cryptography
open FSharp.Collections.ParallelSeq

//TODO: Zip,rar,iso,7z deep inspect

let calcCheckSum (x:FileInfo) =
    try
        let checksum = (new MD5CryptoServiceProvider()).ComputeHash(new BufferedStream(File.OpenRead(x.FullName), 1200000));
        BitConverter.ToString(checksum).Replace("-", String.Empty);
    with
        | :? System.IO.PathTooLongException -> printfn "Path too long! %s" x.Name; ""
    
[<EntryPoint>]
let main argv = 
    let rootDir = if argv.Length > 0 then argv.[0] else Environment.CurrentDirectory
    printfn "%s\n" rootDir
    let xs = (new DirectoryInfo(rootDir) ).EnumerateFiles ("*", SearchOption.AllDirectories)
    let ys = PSeq.map (fun x -> (calcCheckSum x, x.FullName) ) xs   
    let zs = PSeq.groupBy (fun x -> fst x) ys
    let qs = Seq.where (fun y -> Seq.length (snd y) > 1) zs    
    Seq.iter (fun a -> printfn "%s:\n%s" (fst a) (Seq.fold (fun s e -> s + "\t"+ (snd e)+"\n") "" (snd a))) qs
    Console.ReadKey true |> ignore
    0
