﻿open System
open System.IO
open System.Linq
open System.Security.Cryptography

let provider = new MD5CryptoServiceProvider()

let calcCheckSum (x:FileInfo) =
    try
        let checksum = provider.ComputeHash(new BufferedStream(File.OpenRead(x.FullName), 1200000));
        BitConverter.ToString(checksum).Replace("-", String.Empty);
    with
        | :? System.IO.PathTooLongException -> printfn "Path too long! %s" x.Name; ""

let checksum rootDir sumFile =
    let xd = new DirectoryInfo(rootDir);
    let xs = xd.EnumerateFiles ("*", SearchOption.AllDirectories)

    let stream = new StreamWriter(sumFile, false)
    Seq.iter (fun x -> stream.WriteLine(((calcCheckSum x) + " "+ x.FullName.Substring(xd.FullName.Length+1) )) ) xs
    stream.Close()


let verify rootDir sumFile =
    let splitLine line =
        let x = String.split [|' '|] line
        (x.[0],x.[1])

    let readLines = File.ReadLines(sumFile)
    let xs = Seq.map splitLine readLines
    Seq.iter (fun (x,y) ->  if (calcCheckSum (new FileInfo(rootDir+y))) = x then printfn "%s OK!" y else printfn "*** %s DEFECT!" y ) xs

let printWrongCommandMessage =
    printfn "Bad arguments. Try one of the following:"
    printfn "  checksum <rootDir> <sumFile>"
    printfn "  verify <rootDir> <sumFile>"

[<EntryPoint>]
let main argv =     
    if argv.Length = 3 then
        let action = argv.[0]
        let sumFile = argv.[1]
        let rootDir = argv.[2]

        match action with
        | "checksum" -> (checksum rootDir sumFile)
        | "verify" -> (verify rootDir sumFile)
        | _ -> printWrongCommandMessage

        printfn "END OF TRANSMISSION"
        Console.ReadKey true |> ignore
        0
    else
        printWrongCommandMessage
        -1