﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace satel2paxton
{
    internal class Program
    {
        public static bool SurNameFirst = true;

        // NOTE Paxton net2 das not allow PINs to be longer than 4 digits
        private static void Main(string[] args)
        {
            const string satelFile = @"satel.csv";
            const string net2File = @"net2.csv";
            const string net2Header =
                @"Surname, Firstname, Middlename, CardNo, PIN, Department, Access level, Telephone, Extension, Fax, Active date, Expiry date, Field1_100, Field2_100, Field3_50, Field4_50, Field5_50, Field6_50, Field7_50, Field8_50, Field9_50, Field10_50, Field11_50, Field12_50, Field13_Memo, Field14_50, UserID";
            string[] allLines = File.ReadAllLines(satelFile, Encoding.GetEncoding(1252));
            IEnumerable<PaxtonNet2User> query = from line in allLines.Skip(1)
                                                let data = line.Split(';')
                                                select
                                                    new PaxtonNet2User(data[0].Trim(), data[4].Trim(),
                                                                       "Jederzeit an allen Zutrittspunkten" /*data[5]*/,
                                                                       data[7].Trim(),
                                                                       ConvertTransponder(data[12].Trim()));

            using (var file = new StreamWriter(net2File, false, Encoding.GetEncoding(1252)))
            {
                file.WriteLine(net2Header);
                foreach (PaxtonNet2User p in query)
                {
                    Console.WriteLine(p);
                    file.WriteLine(p);
                }
            }
        }

        private static string ConvertTransponder(string hexTransponder)
        {
            if (hexTransponder.Length != 0) {
                var converted = (Int64.Parse(hexTransponder, NumberStyles.HexNumber)%4294967296).ToString();
                if (converted.Length > 8) {
                    Console.WriteLine(converted);
                    converted = converted.Substring(converted.Length - 8);
                    Console.WriteLine(converted);
                }
                return converted;
            }
            else return "";
        }
    }
}