using System.Text;

namespace satel2paxton
{
    internal class PaxtonNet2User
    {
        private readonly string[] _fields;

        public PaxtonNet2User(string userId, string name, string group, string pin, string transponder)
        {
            _fields = new string[27]
                          {
                              "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
                              ,
                              "", "", "", ""
                          };

            if (name.Contains(" "))
            {
                string[] parts = name.Split(new[] {' '}, 2);
                if (Program.SurNameFirst)
                {
                    _fields[0] = parts[0];
                    _fields[1] = parts[1];
                }
                else
                {
                    _fields[0] = parts[1];
                    _fields[1] = parts[0];
                }
            }
            else
            {
                _fields[0] = name;
            }

            _fields[3] = transponder;
            _fields[4] = pin;
            _fields[6] = group;
            _fields[26] = userId;
        }

        public override string ToString()
        {
            var line = new StringBuilder();
            foreach (string field in _fields)
            {
                if (field != _fields[0])
                {
                    line.Append(',');
                }
                line.Append('"');
                line.Append(field.Trim());
                line.Append('"');
            }
            return line.ToString();
        }
    }
}